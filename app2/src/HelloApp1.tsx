import * as React from "react";
import NavBar from "./Routes/nav";
import Routes from "./Routes/Routes";

const HelloApp1 = () => {
    return (
        <React.Suspense fallback="Loading HelloApp1">
            {/* <NavBar/> */}
        <Routes />
      </React.Suspense>
    )
}

export default HelloApp1;
