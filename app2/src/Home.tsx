import * as React from 'react';

const Container = React.lazy(() => import('app1/Container'))
const Home = () => {
    const token = localStorage.getItem('auth_token');
   if(!token) {
       location.reload();
   }
    const showToken = () => {
        alert(token)
    }
    return (
        <React.Suspense fallback="loading ....">

            <Container>

            </Container>
            <h3>welocome to Teachers Info Page {token}</h3>
            <button onClick={showToken}>show </button>
        </React.Suspense>
    )
}

export default Home;