import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./App";
import HelloApp1 from "./HelloApp1";

ReactDOM.render(<HelloApp1 />, document.getElementById("root"));
