import * as React from 'react';
import {   Link } from "react-router-dom";

const styleObj = {
    ul: {
        display: 'flex',
        listStyleType: 'none',
        paddingLeft: '0px'
    },
    li: {
        padding: '10px 20px'
    }
}

const NavBar = () => {
    return(
        <div>
                <ul style={styleObj.ul}>
                    <li style={styleObj.li}>
                        <Link to="/teachers">Home</Link>
                    </li>
                    <li style={styleObj.li}>
                        <Link to="/teachers/english">English</Link>
                    </li>
                    <li style={styleObj.li}>
                        <Link to="/teachers/science">Science</Link>
                    </li>
                    <li style={styleObj.li}>
                        <Link to="/teachers/social">Social</Link>
                    </li>
                </ul>

                <hr />
        </div>
    )
}

export default NavBar;