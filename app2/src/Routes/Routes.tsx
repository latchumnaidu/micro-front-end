import * as React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom"; 
import English from "../English";
import Home from "../Home";
// import Login from "../Login";
import Science from "../Science";
import Social from "../Social";
import PrivateRoute from "./authRoutes"; 

const Routes = () => {
    return (
        <Router  >
            <div>
                <Switch>
                    <PrivateRoute exact path="/teachers" component = {Home} />
                    <PrivateRoute exact path="/teachers/english" component = {English} />
                    <PrivateRoute exact path="/teachers/science" component = {Science} />
                    <PrivateRoute exact path="/teachers/social" component = {Social} />
                  
                </Switch>
            </div>
        </Router>
    );
}



export default Routes;