

import * as React from 'react';
import { FC } from 'react';
import { Route, RouteComponentProps, RouteProps, Redirect } from 'react-router';

import NavBar from './nav';

const PrivateRoute: FC<RouteProps> = (routerProps:RouteProps) => {
    if (!routerProps.component) {
      return null;
    }
    const isLoggedIn = true;
    return (
        <div>
           <NavBar></NavBar>
            <Route
            //   {...rest}
                  render={(props: RouteComponentProps<{}>) => isLoggedIn ? (<routerProps.component {...props} />) : (<Redirect to={{ pathname: '/', state: { from: props.location } }} />)}
            />
        </div>
    );
  };

export default PrivateRoute;