import * as React from "react";
import Routes from "./Routes/Routes";
 
// const RemoteButton = React.lazy(() => import("app2/HelloApp1"));
// const Login = React.lazy(() => import("app2/Login"));

const App = () => {
    	
  
  return (
  
    <div>
     
       
      <React.Suspense fallback="Loading HelloApp1">
        <Routes />
      </React.Suspense>
     
    </div>
  )
}

export default App;
