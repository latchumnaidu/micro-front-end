import * as React from 'react';
import {   Link } from "react-router-dom";

const styleObj = {
    ul: {
        display: 'flex',
        listStyleType: 'none',
        paddingLeft: '0px'
    },
    li: {
        padding: '10px 20px'
    }
}

const NavBar = () => {
    return(
        <div>
                <ul style={styleObj.ul}>
                    <li style={styleObj.li}>
                        <Link to="/landingpage">landingpage</Link>
                    </li>
                    <li style={styleObj.li}>
                        <Link to="/teachers">Teacher Info</Link>
                    </li>
                    <li style={styleObj.li}>
                        <Link to="/students_info">Students Info</Link>
                    </li>
                    <li style={styleObj.li}>
                        <Link to="/">LogOut</Link>
                    </li>
                </ul>

                <hr />
        </div>
    )
}

export default NavBar;