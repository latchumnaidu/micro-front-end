import * as React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import LandingPage from '../LandingPage';
import Login from "../Login";
import PrivateRoute from "./authRoutes";
const Home = React.lazy(() => import('app2/HelloApp1'));
const StudentsInfo = React.lazy(() => import('app3/StudentsInfo'));

const Routes = () => {
    return (
        <Router  >
            <div>
                <Switch>
                    <Route exact path="/" component = {Login} />
                    <PrivateRoute exact path="/landingpage" component={LandingPage} />
                    <PrivateRoute exact path="/teachers" component={Home} />
                    <PrivateRoute exact path="/students_info" component={StudentsInfo} />
                </Switch>
            </div>
        </Router>
    );
}



export default Routes;