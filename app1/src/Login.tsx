import * as React from 'react';
import { withRouter } from "react-router-dom";

const styleObj = {
    login: {
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
    // logincontrol: {

    // }
    // btncontrol :{

    // }
}
// const DivContainer = React.lazy(() => import("app3/DivContainer"));
const Login = withRouter(({ history }) => {
    const onSubmit = () => {
        const modal:HTMLElement = document.getElementById("myModal");
        modal.style.display = "flex";
        setTimeout(() => {
            if (history) history.push('/landingpage');
            modal.style.display = "none";
        }, 2000);
    }
    return (
        <div style={styleObj.login}>
            <style>{`
        label{
          display: block;
        
        }
        input {
            padding: 10px;
            margin-bottom: 10px;
            width: 100%
        }
       
.modal {
  display: none; 
  position: fixed; 
  z-index: 1;   
  left: 0;
  top: 0;
  width: 100%;  
  height: 100%;  
  overflow: auto;  
  background-color: rgb(0,0,0);  
  background-color: rgba(0,0,0,0.4);  
}

 
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 20%;
}
        
      `}</style>
            <div>
                <div >
                    <label>User Name</label>
                    <input type="text" />
                </div>
                <div >
                    <label>Password</label>
                    <input type="text" />
                </div>
                <div>
                    <input type="button" value="Submit" onClick={onSubmit} />
                </div>
                <div id="myModal" className="modal">
                     <div className="modal-content">
                          Loading .......
                    </div>

                </div>
            </div>
        </div>

    )
})

export default Login;