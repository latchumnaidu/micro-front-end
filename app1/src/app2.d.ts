 

declare module "app2/HelloApp1" {
  const App1Component: React.ComponentType;

  export default App1Component;
}

declare module "app2/Login" {
  const App1Login: React.ComponentType;

  export default App1Login;
}

declare module "app3/StudentsInfo" {
  const StudentsInfo: React.ComponentType;

  export default StudentsInfo;
}