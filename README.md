 
Clone down this repository. You will need node and npm installed globally on your machine.

Installation:

npm install

To Start Application: (run all applications )

npm start

To Visit App:

localhost:8080


packages:

webpack module federation plugin ( to expose application and use remotely)
bundel-loader (to lazy load of bundle by importing in index.ts )

description: 

any exposed component will be available in below predefined format


name in module federation + @http:// + url + remoteEntry.js

Eg:  app1@http://localhost:8080/remoteEntry.js

to use exposed component: 

if you are using typescript
you should add a typing for remote component path

remote component will  be available in below format 

name in module federation + exposed name

Eg:  app2/Login

in app2.d.ts file we are declaring type module

after that we can import directly from this path or we can use react lazy loading

if we use react lazy loading we should use react suspense for error handling

State in apps:::

if we want to share a token or any information through out applications
created a component with empty div and set a localstore item
when it calls in any application  this localstorage will be availabe there also.
Eg: app1  > comtainer.tsx
Eg: app2  > home.tsx
